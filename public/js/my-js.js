var arrayExerciseOne = [10,20,30];
function sumForLoop(arrayNumber){
	var sum = 0;
	for (var i = 0; i < arrayNumber.length; i++){
		sum = sum + arrayNumber[i];
	}
	return sum;
}
function sumWhileLoop(arrayNumber){
	var sum = 0;
	var i = 0;
	while(i < arrayNumber.length){
		sum = sum + arrayNumber[i];
		i++;
	}
	return sum;
}

var arrayExerciseTwoA = [10,20,30];
var arrayExerciseTwoB = ['a','b','c'];
function mergeArray(arrayA, arrayB) {
	var array =[];
	for (var i = 0; i < arrayA.length || i < arrayB.length; i++){
		if (i < arrayA.length){
			array.push(arrayA[i]);
		}
		if (i < arrayB.length){
			array.push(arrayB[i]);
			}
		}
	return array;
}

function fibonacci(x){
	var x1 = 0, x2 = 1, x3, arrayfibonacci = [0,1];
	for (var i = 2; i < x; i++){
		x3 = x1 + x2;
		arrayfibonacci.push(x3);
		x1 = x2;
		x2 = x3;
	}
	return arrayfibonacci;
}

var arrayExerciseFour = [56,5, 5, 53, 68, 58];
function sortArray(array){
		array.sort();
		array.reverse();
		function compareNumber(a, b){
			return (b.toString() + a.toString()) - (a.toString() + b.toString());
		}
		array.sort(compareNumber);
		return Number(array.join(""));
	}

/* function solution5(accumulated, total, ...numbers) {
  if (numbers.length === 0) {
    if (total == 100) alert(accumulated);
  }
  else {
    const [first, ...butFirst] = numbers;
    if (accumulated !== '') {
      // TH 1, điền dấu +
      solution5(`${accumulated}+${first}`, total + first, ...butFirst);
      // TH 2, điền dấu -
      solution5(`${accumulated}-${first}`, total - first, ...butFirst);
    }
    else solution5(`${first}`, first, ...butFirst);
    // TH 3, không điền dấu
    if (butFirst.length > 0) {
      const [second, ...butSecond] = butFirst;
      solution5(accumulated, total, first * 10 + second, ...butSecond);
    }
  }
} */

function checkNumber(numb){
	if (numb < 2){
	return "Số vừa nhập không phải là số nguyên tố";
	}
	else {
	var check1 = true;
	for (let i = 2; i < numb; i++){
		if ( (numb%i) == 0){
			check1 = false;
		}
	}
	if(check1) {
		return "Số vừa nhập là số nguyên tố";
	}else return "Số vừa nhập không phải là số nguyên tố";
	}
}

function resultCasio(inputString){
	return eval(inputString);
}
function getContent(id){
	return document.getElementById(id).textContent;
}
function clearNumber(id1, id2){
	document.getElementById(id1).innerHTML = "" ;
	document.getElementById(id2).innerHTML = "" ;
}